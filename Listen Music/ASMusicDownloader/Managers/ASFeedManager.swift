//
//  ASFeedManager.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/2/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

let clientID = "717ce0a093aae061dbdabbad225fa30e"

typealias CompletionBlock = (feeds:NSArray?, error:NSError?) -> ()

class SearchResult:NSObject {
    var searchString:NSString!
    var searchIndex:Int!
    var searchType:SearchType!
    
    init(content:NSString!, index:Int!, type:SearchType!) {
        
        searchString = content
        searchIndex = index
        searchType = type
    }
}

class ASFeedManager: NSObject {
    
    internal var itemInfo:NSMutableDictionary?
    internal var currentKey:NSString?
    internal var responseBlock:CompletionBlock?

    internal var feeds:NSMutableArray?
    
    internal var reqManger:AFHTTPRequestOperationManager!
    internal var reqOperation:AFHTTPRequestOperation!
    
    internal var inprogress:Bool = false
    
    class var sharedInstance: ASFeedManager {
        struct Static {
            static var instance: ASFeedManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASFeedManager()
        }
        
        return Static.instance!
    }
    
    override init() {
        super.init()
        reqManger = AFHTTPRequestOperationManager()
        var serlizer = AFJSONResponseSerializer()
        serlizer.acceptableContentTypes = NSSet(object: "application/json")
        serlizer.stringEncoding = NSUTF8StringEncoding
        reqManger.responseSerializer = serlizer
    }

    func cancelOperation () {
        if inprogress {
            reqOperation.cancel()
        }
    }
    
    func loadFeed(query:NSString?, finishBlock:CompletionBlock?) {
        
        responseBlock = finishBlock
        var feedURL:String
        if let q = query {
            feedURL = "https://api.soundcloud.com/tracks.json?client_id=" + clientID + "&q=" + q + "&limit=20"
        } else {
            feedURL = "http://wv.smarnovative.com/YouTube-Downloader-master/latestsounds.php"
        }
        
        var url = NSURL(string: feedURL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        var urlRequest = NSURLRequest(URL: url!)
        
        cancelOperation()
        
        reqOperation = reqManger.HTTPRequestOperationWithRequest(urlRequest, success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
            
            self.inprogress = false
            
            var responseArray = responseObject as NSArray
            
            self.feeds = NSMutableArray()
            
            for info in responseArray {
                
                var media = ASMedia(info: info as NSDictionary, type: MediaType.Audio)
                
                self.feeds?.addObject(media)
            }
            
            if let block = finishBlock {
                block(feeds: self.feeds, error: nil)
            }
            
        }, failure:{ (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
            
            self.inprogress = false
            if let block = finishBlock {
                if !operation.cancelled {
                    block(feeds: nil, error: error)
                }
            }
        })
        reqOperation.start()
        inprogress = true
    }
}

