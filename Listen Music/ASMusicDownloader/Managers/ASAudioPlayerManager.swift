//
//  ASAudioPlayerManager.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/30/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

enum ASPlayerStatus {
    
    case play
    case pause
    case Default
}

protocol ASAudioPlayerDelegate {
    
    func playerTimeProgress(left:Float, duration:Float, current:Float)
    func playerChangeStatus()
    func playerReceiveError()
}

class ASAudioPlayerManager: NSObject {
    
    var audioPlayer:AVQueuePlayer!
    
    var status = ASPlayerStatus.Default
    
    var progressObserver:AnyObject!
    
    var delegate:ASAudioPlayerDelegate?
    
    var currentMedia:ASMedia?
   
    class var sharedInstance: ASAudioPlayerManager {
        struct Static {
            static var instance: ASAudioPlayerManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ASAudioPlayerManager()
        }
        
        return Static.instance!
    }
    
    override init() {
        
        super.init()
        var session = AVAudioSession.sharedInstance()
        session.setCategory(AVAudioSessionCategoryPlayback, error: nil)
        session.setActive(true, error: nil)
        
        audioPlayer = AVQueuePlayer()
        
        audioPlayer.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.allZeros, context: nil)
        
        var interval = CMTimeMake(1, 1)
        progressObserver = audioPlayer.addPeriodicTimeObserverForInterval(interval, queue: dispatch_get_main_queue(), usingBlock: {(var time:CMTime) -> () in
            
            if self.audioPlayer.currentItem != nil {
                var timeleft = (CMTimeGetSeconds(self.audioPlayer.currentItem.duration) - CMTimeGetSeconds(self.audioPlayer.currentItem.currentTime()));
                var duration = CMTimeGetSeconds(self.audioPlayer.currentItem.duration);
                var currentTime = CMTimeGetSeconds(self.audioPlayer.currentItem.currentTime());
                if duration > 0 && duration < Float64.infinity {
                    if currentTime < 1 {
                        if duration < Float64.infinity {
                            self.setDurationInfo(Int(duration))
                        }
                    }
                    
                    if let del = self.delegate {
                        del.playerTimeProgress(Float(timeleft), duration: Float(duration), current: Float(currentTime))
                    }
                }
            }
        })
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerItemDidReachEnd:", name: AVPlayerItemDidPlayToEndTimeNotification, object: audioPlayer.currentItem)

    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: audioPlayer.currentItem)
    }
    
    
    func addMediaList(list:Array<ASMedia>) {
        
        self.audioPlayer.removeAllItems()
        for media in list {
            var asset = AVURLAsset(URL: media.getStreamURL(), options: nil)
            var keys = ["playable"];
            
            asset.loadValuesAsynchronouslyForKeys(keys, completionHandler: {() -> () in
                dispatch_async(dispatch_get_main_queue(), {() -> () in

                    var playerItem = AVPlayerItem(asset: asset)
                    self.audioPlayer.insertItem(playerItem, afterItem: nil)
                    
                    if self.audioPlayer.items().count == list.count {
                        self.play()
                        println("Start Play")
                    } 
                })
            })

            
        }
        
    }
    func playMedia(media:ASMedia) {
        
        ASHistoryManager.sharedInstance.saveMediaItems(media)
        currentMedia = media
        
        println("Play Now: \(media.getStreamURL())")
        
        var asset = AVURLAsset(URL: media.getStreamURL(), options: nil)
        var keys = ["playable"];
        
        asset.loadValuesAsynchronouslyForKeys(keys, completionHandler: {() -> () in
            dispatch_async(dispatch_get_main_queue(), {() -> () in
                var playerItem = AVPlayerItem(asset: asset)
                if self.audioPlayer.canInsertItem(playerItem, afterItem: nil) {
                    self.audioPlayer.removeAllItems()
                    self.audioPlayer.insertItem(playerItem, afterItem: nil)
                    self.play()
                    self.status = ASPlayerStatus.play
                    
                    self.setPlayerInfo()
                    AsyncImageLoader.sharedLoader().loadImageWithURL(media.avatarLink, target: self, success: "avatarLoadingSuccess:", failure: "avaterLoadingFail:")
                    if let del = self.delegate {
                        del.playerChangeStatus()
                    }
                } else {
                    if let del = self.delegate {
                        del.playerReceiveError()
                    }
                }
            })
        })
    }
    
    func avatarLoadingSuccess(image:UIImage!) {
        if image != nil {
            
            setAvatarMedia(image)
        }
        println("Loading Sccess");
    }
    func avaterLoadingFail(error:NSError) {
        println("error: \(error.localizedDescription)")
    }
    
    func setAvatarMedia(image:UIImage) {
        
        if let media = currentMedia {
            println("Set Avatar")
            var artWork = MPMediaItemArtwork(image:image)
            var mediaInfo = MPNowPlayingInfoCenter.defaultCenter()
            var info = mediaInfo.nowPlayingInfo
            info[MPMediaItemPropertyArtwork] = artWork
            mediaInfo.nowPlayingInfo = info
        }
    }
    
    func setDurationInfo(duration:Int) {
        
        if let media = currentMedia {
            
            var mediaInfo = MPNowPlayingInfoCenter.defaultCenter()
            var info = mediaInfo.nowPlayingInfo
            if info[MPMediaItemPropertyPlaybackDuration] == nil {
                println("Set Duration")
                info[MPMediaItemPropertyPlaybackDuration] = duration
                mediaInfo.nowPlayingInfo = info
            }
        }
    }

    func setPlayerInfo () {
        
        if let media = currentMedia {
            println("Set Player Info")
            var mediaInfo = MPNowPlayingInfoCenter.defaultCenter()
            
            var info:[NSObject:AnyObject!] = [MPMediaItemPropertyTitle:media.mediaTitle,
                                            MPMediaItemPropertyArtist:media.userName]
            
            mediaInfo.nowPlayingInfo = info
        }
        
        
    }
    
    func play() {
        if audioPlayer.currentItem == nil {
            if let media = currentMedia {
                playMedia(media)
            }
        } else {
            audioPlayer.play()
            status = ASPlayerStatus.play
            if let del = self.delegate {
                del.playerChangeStatus()
            }

        }
    }
    func pause() {
        audioPlayer.pause()
        status = ASPlayerStatus.pause
        if let del = self.delegate {
            del.playerChangeStatus()
        }
    }
    
    func isPlaying() -> Bool {
        println("Rate: \(audioPlayer.rate)")
        return status == ASPlayerStatus.play
    }
    
    func moveToSeekTime(value:Float64) {
        if audioPlayer.currentItem == nil {
            if let del = self.delegate {
                del.playerTimeProgress(1, duration: 1, current: 0)
            }
        } else {
            var duration = CMTimeGetSeconds(self.audioPlayer.currentItem.duration)
            var seekTime = duration*value
            audioPlayer.seekToTime(CMTimeMake(Int64(seekTime), 1))
        }
    }
    
    func getCurrentItemURL () -> NSURL? {
        
        if audioPlayer.currentItem != nil {
            var asset = audioPlayer.currentItem.asset as AVURLAsset;
            return asset.URL
        }
        return nil
    }
    
    func playerItemDidReachEnd(notification:NSNotification) {
        
        println("EndPlay")
        status = ASPlayerStatus.Default
        
        if let del = self.delegate {
            del.playerChangeStatus()
        }
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        if (keyPath == "status") {
            
            switch audioPlayer.status {
            case AVPlayerStatus.ReadyToPlay:
                println("Ready To Play")
                break
            case AVPlayerStatus.Failed:
                println("Failed")
                break
            default:
                println("Default")
                break
            }
        }
    }

    
}

