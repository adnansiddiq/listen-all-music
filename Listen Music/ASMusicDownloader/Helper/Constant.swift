//
//  Constant.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/2/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

struct MyConstants {
    
    static var kCacheFeedResponse = "cacheFeed"
    static var kVideoCacheResponse = "videoCache"
}

enum SearchType {
    case Feed
    case Video
    case PDF
}

enum MyKeys : Int {
    case KeyTitle, KeyAuthor, KeyDescription, KeyLink, KeyPubDate
    func toKey() -> String! {
        switch self {
        case .KeyLink:
            return "link"
        case .KeyDescription:
            return "description"
        case .KeyPubDate:
            return "pubDate"
        case .KeyAuthor:
            return "author"
        case .KeyTitle:
            return "title"
        default:
            return ""
        }
    }
}

extension NSUserDefaults {
    
    class func saveObject(object:AnyObject, key:String) {
        
        NSUserDefaults.standardUserDefaults().setObject(object, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func getObject(key:String) -> AnyObject? {
        
        return NSUserDefaults.standardUserDefaults().objectForKey(key)
    }
}

extension Array {
    
    mutating func removeObject<U: Equatable>(object: U) {
        var index: Int?
        for (idx, objectToCompare) in enumerate(self) {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }
        
        if index != nil {
            self.removeAtIndex(index!)
        }
    }

}

