//
//  UIStoryboard_Helper.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/1/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

extension UIStoryboard {
    
    class func mainStoryboad () -> UIStoryboard {
        
        return UIStoryboard(name: "Main", bundle: nil)
    }
    class func viewControllerWithIdentifair (identifair:NSString) -> AnyObject {
        
        var mainBoard = UIStoryboard.mainStoryboad()
        var viewController:AnyObject = mainBoard.instantiateViewControllerWithIdentifier(identifair)
        return viewController
    }
}