//
//  NSDate_Helper.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/7/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

extension NSDate {
    
    func stringWithFormate(formate:String) -> String {
        
        var dateFormater = NSDateFormatter()
        dateFormater.dateFormat = formate
        
        return dateFormater.stringFromDate(self)
    }
    
    class func dateWithString(string:NSString, format:NSString) -> NSDate! {
     
        var dateFormater = NSDateFormatter()
        dateFormater.dateFormat = format
        
        return dateFormater.dateFromString(string)

    }
}
