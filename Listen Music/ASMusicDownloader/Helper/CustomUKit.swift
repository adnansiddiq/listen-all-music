//
//  CustomUKit.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/8/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit


class MyLabel:UILabel {
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        if iOS7 {
            if self.numberOfLines == 0 {
                if self.preferredMaxLayoutWidth != self.frame.size.width {
                    self.preferredMaxLayoutWidth = self.frame.size.width
                    self.setNeedsUpdateConstraints()
                }
                
            }
        }
    }
    override func intrinsicContentSize() -> CGSize {
        
        var size = super.intrinsicContentSize()
        
        if (numberOfLines == 0 && iOS7){
            size.height+=1
        }
        return size
    }
}
