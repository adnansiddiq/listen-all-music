//
//  String_Helper.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/4/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation


extension String {
    
    func terminatingWitheSpaceAndNewLine() -> NSString {
        
        var temp = self as NSString
        var result = temp.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        return result
    }
    
    func dateWithFormate(formate:NSString) -> NSDate? {
        
        var dateFormater = NSDateFormatter()
        dateFormater.dateFormat = formate
        
        return dateFormater.dateFromString(self)
    }
    
    static func documentDirctoryWithPath(path:String) -> String {
    
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true) as NSArray
        var documentsDirectory = paths.objectAtIndex(0) as NSString
        documentsDirectory.stringByAppendingPathComponent(path)
        
        return documentsDirectory.stringByAppendingPathComponent(path)
    }
    
    static func timeFormatted(time:Int) -> String! {
        var seconds = time % 60
        var minutes = (time / 60) % 60
        var hours = time / 3600
        if hours != 0 {
            return NSString(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return NSString(format: "%02d:%02d", minutes, seconds)
        }
    }
}