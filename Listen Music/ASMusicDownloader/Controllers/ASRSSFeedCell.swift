//
//  ASRSSFeedCell.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/7/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

class ASRSSFeedCell: UITableViewCell {

    @IBOutlet weak var feedTitle: UILabel!
    @IBOutlet weak var feedDetail: UILabel!
    @IBOutlet weak var feedDate: UILabel!
    @IBOutlet weak var totalViews: UILabel!
    
    @IBOutlet weak var downloadLabel:UILabel!
    
    @IBOutlet weak var avatar:AsyncImageView!
    
//    override func layoutSubviews() {
//
//        contentView.setNeedsLayout()
//        contentView.layoutIfNeeded()
//        
//        super.layoutSubviews()
//        
//    }
}
