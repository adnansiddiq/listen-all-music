//
//  ASDownloadsViewController.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/22/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

class ASDownloadsViewController: ASBaseViewController {
    
    @IBOutlet weak var downloadList:UITableView!
    
    @IBOutlet weak var infoView:UIView!
    
    internal var actionSheet:UIActionSheet?
    
    internal var downloads = Array<ASMedia>()
    
    var selectedMedia:ASMedia!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Downloads"
        self.downloadList.tableFooterView = UIView(frame: CGRectZero)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "downloadComplete:", name: DownloadCompleteNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        downloads = ASDownloadManager.sharedInstance.downloadedItem
        self.infoView.hidden = downloads.count != 0
        
        downloadList.reloadData()
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: DownloadCompleteNotification, object: nil)
    }
    
    func downloadComplete(notification:NSNotification) {
        
        var media = notification.object as ASMedia
        
        let index = find(downloads, media)
        downloads.removeObject(media)
        
        downloadList.beginUpdates()
        downloadList.deleteRowsAtIndexPaths([NSIndexPath(forRow: index!, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Left)
        downloadList.endUpdates()
        
        self.infoView.hidden = downloads.count != 0
        
        if let action = actionSheet {
            action.dismissWithClickedButtonIndex(0, animated: true)
        }
    }
        
    @IBAction func searchSeactionAction(sender:UIButton) {
        
        if let tabbarController = self.tabBarController {
            tabbarController.selectedIndex = 0
        }
    }

}

extension ASDownloadsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.downloads.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var downloadCell = tableView.dequeueReusableCellWithIdentifier(downloadCellID) as ASDownloadCell
        
        var media = downloads[indexPath.row] as ASMedia
        
        downloadCell.configureCell(media);
        
        return downloadCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedMedia = downloads[indexPath.row] as ASMedia
        
        if selectedMedia.mediaStatus == MediaStatus.Error || selectedMedia.mediaStatus == MediaStatus.None {
            actionSheet = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles:"Start", "Delete", "Cancel")
        } else if selectedMedia.mediaStatus == MediaStatus.InProgress {
            actionSheet = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles:"Pause", "Stop", "Cancel")
        } else if selectedMedia.mediaStatus == MediaStatus.pause {
            actionSheet = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles:"Resume", "Delete", "Cancel")
        }
        if let action = actionSheet {
            action.destructiveButtonIndex = 1
            action.cancelButtonIndex = 2
            action.showFromTabBar(self.tabBarController?.tabBar)
        }

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
//        if media.mediaStatus == MediaStatus.Error || media.mediaStatus == MediaStatus.None {
//        } else {
//            
//        }
        
    }
}

extension ASDownloadsViewController:UIActionSheetDelegate {
    
    func deleteMedia(media:ASMedia) {
        media.stopOperation()
        ASDownloadManager.sharedInstance.removeItem(media)
        downloads = ASDownloadManager.sharedInstance.downloadedItem
        
        if let tabvc = self.tabBarController {
            var temp = tabvc as MyTabbarController
            temp.setBadgeValue()
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        println("Action Sheet: \(buttonIndex)")
        
        if selectedMedia.mediaStatus == MediaStatus.Error || selectedMedia.mediaStatus == MediaStatus.None {
            if buttonIndex == 0 {
                selectedMedia.downloadMedia()
            } else if buttonIndex == 1 {
                deleteMedia(selectedMedia)
            }

        } else if selectedMedia.mediaStatus == MediaStatus.InProgress {
            if buttonIndex == 0 {
                selectedMedia.pauseOperation()
            } else if buttonIndex == 1 {
                selectedMedia.stopOperation()
            }
        } else if selectedMedia.mediaStatus == MediaStatus.pause {
            if buttonIndex == 0 {
                selectedMedia.resumeOperation()
            } else if buttonIndex == 1 {
                deleteMedia(selectedMedia)
            }
        }
        if buttonIndex != 2 {
            self.downloadList.reloadData()
        }
    }
    func actionSheetCancel(actionSheet: UIActionSheet) {
        println("Action Cancel")
    }
    
}