//
//  ASAudioPlayerViewController.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/23/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit
import AVFoundation

class ASAudioPlayerViewController: ASBaseViewController {
    
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var songNameLabel:UILabel!
    @IBOutlet weak var progressSlider:UISlider!
    @IBOutlet weak var avatarImageView:AsyncImageView!
    
    @IBOutlet weak var currentTimeLabel:UILabel!
    @IBOutlet weak var durationLabel:UILabel!
    
    var media:ASMedia!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Player"
        
        ASAudioPlayerManager.sharedInstance.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if ASHistoryManager.sharedInstance.recentItems.count == 0 {
            self.navigationItem.rightBarButtonItem?.enabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
        
        self.playButton.selected = ASAudioPlayerManager.sharedInstance.isPlaying();
        
        if let temp = ASAudioPlayerManager.sharedInstance.currentMedia {
            media = temp
            setControlValues()
        }
    }
    
    func setControlValues() {
        self.songNameLabel.text = media.mediaTitle
        avatarImageView.image = UIImage(named: "cover_icon")
        avatarImageView.imageURL = media.avatarLink
        
        if let index = ASHistoryManager.sharedInstance.indexOfMedia(media) {
            self.navigationItem.title = "\(index) of \(ASHistoryManager.sharedInstance.recentItems.count)"
        }
        self.progressSlider.value = 0
    }
    
    @IBAction func playPauseAction(sender: UIButton) {
        if media == nil {
            if ASHistoryManager.sharedInstance.recentItems.count > 0 {
                media = ASHistoryManager.sharedInstance.recentItems[0] as ASMedia
                ASAudioPlayerManager.sharedInstance.playMedia(media)
                setControlValues()
            } else {
                return
            }
        }
        if !sender.selected {
            ASAudioPlayerManager.sharedInstance.play()
        } else {
            ASAudioPlayerManager.sharedInstance.pause()
        }
        
    }
    @IBAction func nextSongAction(sender: AnyObject) {
        
        if media == nil {
            return
        }
        
        if let temp = ASHistoryManager.sharedInstance.mediaItemAfter(media) {
            ASAudioPlayerManager.sharedInstance.pause()
            ASAudioPlayerManager.sharedInstance.playMedia(temp);
            media = temp
            setControlValues()
            
            self.showProgressView(self.view, message: "Loading...")
        }
    }

    @IBAction func previousSongAction(sender: AnyObject) {
        if media == nil {
            return
        }
        if let temp = ASHistoryManager.sharedInstance.mediaItemBefore(media) {
            ASAudioPlayerManager.sharedInstance.pause()
            ASAudioPlayerManager.sharedInstance.playMedia(temp);
            media = temp
            setControlValues()
            
            self.showProgressView(self.view, message: "Loading...")
        }
    }
    
    @IBAction func seekToTime(sender: UISlider) {
        if media == nil {
            sender.value = 0
            return
        }
        ASAudioPlayerManager.sharedInstance.moveToSeekTime(Float64(sender.value))
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    func timeFormatted(time:Int) -> String! {
        var seconds = time % 60
        var minutes = (time / 60) % 60
        var hours = time / 3600
        if hours != 0 {
            return NSString(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return NSString(format: "%02d:%02d", minutes, seconds)
        }
    }
}

extension ASAudioPlayerViewController:ASAudioPlayerDelegate {
    
    func playerTimeProgress(left:Float, duration:Float, current:Float) {
        self.progressSlider.value = current/duration
        self.currentTimeLabel.text = timeFormatted(Int(current))
        self.durationLabel.text = timeFormatted(Int(duration))
    }
    func playerChangeStatus() {
        self.playButton.selected = ASAudioPlayerManager.sharedInstance.isPlaying();
        self.progressSlider.value = 0;
        
        if media == nil {
            media = ASAudioPlayerManager.sharedInstance.currentMedia
            self.songNameLabel.text = media.mediaTitle
        }
        self.hideProgress()
    }
    
    func playerReceiveError() {
        UIAlertView(title: "", message: "Can't play this song.")
    }
}
