//
//  ASBaseViewController.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/1/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

class ASBaseViewController: UIViewController, SWRevealViewControllerDelegate  {
    
    var beganPosition:CGFloat!
    var myHud:MBProgressHUD?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return UIStatusBarStyle.Default
    }


    @IBAction func backAction(sender:UIButton) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}

extension ASBaseViewController: MBProgressHUDDelegate {
    
    func showProgressView(blockView:UIView!, message:String) {
        
        if myHud == nil {
            myHud = MBProgressHUD.showHUDAddedTo(blockView, animated: true)
            if let hub = myHud {
                hub.delegate = self
                hub.userInteractionEnabled = false
                println("Add")
            }
            myHud?.removeFromSuperViewOnHide = true
        }
        myHud?.labelText = message
    }
    
    func hideProgress() {
        myHud?.hide(true)
    }
    
    func hudWasHidden(hud: MBProgressHUD!) {
        myHud = nil
    }
}


class MyTabbarController: UITabBarController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "downloadComplete:", name: DownloadCompleteNotification, object: nil)
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: DownloadCompleteNotification, object: nil)
    }
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
    override func supportedInterfaceOrientations() -> Int {
        
        return Int(UIInterfaceOrientationMask.Portrait.rawValue)
        
    }
    
    override func shouldAutorotate() -> Bool {
        
        return false
    }
    
    func downloadComplete(notification:NSNotification) {
        
        setBadgeValue()
    }
    func setBadgeValue () {
        var vcs = self.viewControllers!
        var vc = vcs[2] as UIViewController
        if ASDownloadManager.sharedInstance.downloadedItem.count == 0 {
            vc.tabBarItem.badgeValue = nil
        } else {
            vc.tabBarItem.badgeValue = "\(ASDownloadManager.sharedInstance.downloadedItem.count)"
        }
    }
}


