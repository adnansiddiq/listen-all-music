//
//  ASMyFilesViewController.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/22/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit
import MediaPlayer

class ASMyFilesViewController: ASBaseViewController {
    
    @IBOutlet weak var sectionSegment: UISegmentedControl!
    @IBOutlet weak var fileList: UITableView!

    @IBOutlet weak var infoView:UIView!

    internal var downloads = Array<ASMedia>()
    internal var selectedMedia:ASMedia!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Files"
        self.fileList.tableFooterView = UIView(frame: CGRectZero)
        
        ASMyFilesManager.sharedInstance.synWithWeb()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "downloadComplete:", name: DownloadCompleteNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        categorySwitchAction(sectionSegment)
        
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: DownloadCompleteNotification, object: nil)

    }
    func downloadComplete(notification:NSNotification) {
        
        var media = notification.object as ASMedia
        
        if ((media.mediaType == MediaType.Audio.toValue() && sectionSegment.selectedSegmentIndex == 0) || (media.mediaType == MediaType.Video.toValue() && sectionSegment.selectedSegmentIndex == 1))
        {
            downloads.insert(media, atIndex: 0)
            
            var indexPaths = Array<NSIndexPath>()
            
            for (var i = 1; i < downloads.count-1; i++) {
                
                indexPaths.append(NSIndexPath(forRow: i, inSection: 0))
            }
            
            fileList.beginUpdates()
            if downloads.count == 1 {
                fileList.insertRowsAtIndexPaths([NSIndexPath(forRow: downloads.count - 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Left)
            } else {
                fileList.insertRowsAtIndexPaths([NSIndexPath(forRow: downloads.count - 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.None)
                fileList.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Left)
                if indexPaths.count != 0 {
                    fileList.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.None)
                }
            }
            fileList.endUpdates()
        }
        self.infoView.hidden = downloads.count != 0
    }
    
    @IBAction func categorySwitchAction(sender: UISegmentedControl) {
        
        if sectionSegment.selectedSegmentIndex == 0 {
            downloads = ASMyFilesManager.sharedInstance.getAudioItems()
        } else {
            downloads = ASMyFilesManager.sharedInstance.getVideoItems()
        }
        self.infoView.hidden = downloads.count != 0
        self.fileList.reloadData()
    }
    
    @IBAction func handleLongGesture(gesture:UILongPressGestureRecognizer) {
        
        var p = gesture.locationInView(fileList)
        if let indexPath = fileList.indexPathForRowAtPoint(p) {
            if gesture.state == UIGestureRecognizerState.Began {
                selectedMedia = downloads[indexPath.row] as ASMedia
                
                var actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Delete", "Cancel")
                actionAlert.destructiveButtonIndex = 1
                actionAlert.cancelButtonIndex = 2
                actionAlert.showFromTabBar(self.tabBarController?.tabBar)

            }
        }
    }

    
    func playMedia(media:ASMedia) {
        if media.mediaType == MediaType.Video.toValue() {
            var savePath = String.documentDirctoryWithPath(media.mediaID + ".mp4")
            var mediaURL = NSURL(fileURLWithPath: savePath)!
            var player = MPMoviePlayerViewController(contentURL: mediaURL)
            
            self.presentMoviePlayerViewControllerAnimated(player)
        } else {
            ASAudioPlayerManager.sharedInstance.playMedia(media)
        }

    }
    
    func deleteMedia(media:ASMedia) {
        ASMyFilesManager.sharedInstance.removeItem(media)
        
        let index = find(downloads, media)
        downloads.removeObject(media)
        
        fileList.beginUpdates()
        fileList.deleteRowsAtIndexPaths([NSIndexPath(forRow: index!, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Left)
        fileList.endUpdates()
        
        self.infoView.hidden = downloads.count != 0

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.destinationViewController.isKindOfClass(ASAudioPlayerViewController) {
            var vc = segue.destinationViewController as ASAudioPlayerViewController
            vc.media = sender as ASMedia
        }
    }
}

extension ASMyFilesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.downloads.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var feedCell = tableView.dequeueReusableCellWithIdentifier("cell") as ASRSSFeedCell
        
        var media = downloads[indexPath.row] as ASMedia
        
        feedCell.feedTitle.text = media.mediaTitle
        feedCell.feedDetail.text = media.userName
        feedCell.feedDate.text = media.fileSizeInString()

        feedCell.avatar.image = UIImage(named: "browse_peer_audio_icon_selector_on")
        feedCell.avatar.cancelLoading()
        if let url = media.avatarLink {
            feedCell.avatar.imageURL = url
        }
        
        return feedCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedMedia = downloads[indexPath.row] as ASMedia
        playMedia(selectedMedia)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let tabbarVC = self.tabBarController {
            tabbarVC.selectedIndex = 3
        }
    }
}

extension ASMyFilesViewController:UIActionSheetDelegate {
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        println("Action Sheet: \(buttonIndex)")
        if buttonIndex == 0 {
            playMedia(selectedMedia)
        } else if buttonIndex == 1 {
            
            UIAlertView(title: "", message: "Are you sure to delete \"\(selectedMedia.mediaTitle)\"?", cancelButtonTitle: "No", otherButtonTitles: ["Yes"], onDismiss: {(buttonIndex:Int32) -> () in
                self.deleteMedia(self.selectedMedia)
                }, onCancel: {() -> () in
            })
        }
    }
    func actionSheetCancel(actionSheet: UIActionSheet) {
        println("Action Cancel")
    }
    
}

