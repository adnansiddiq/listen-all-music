//
//  ASFeed.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/2/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import Foundation

enum MediaType {
    case Audio 
    case Video
    
    static func typeFrom(val:Int) -> MediaType {
        switch val {
        case 1: return MediaType.Audio
        default: return MediaType.Video
        }
    }
    
    func toValue() -> String {
        switch self {
        case .Audio: return "audio"
        case .Video: return "video"
        default: return ""
        }
    }
}

enum MediaStatus {
    case None
    case InProgress
    case pause
    case Completed
    case Error
}

protocol ASMediaDelegate {
    
    func downloadReport(status:MediaStatus)
}

let DownloadCompleteNotification = "complete_download"

class ASMedia: NSObject {
    
    var mediaID:String!
    var mediaTitle:String!
    var mediaDesc:String!
    var mediaStreamUrl:String!
    var mediaDuration:Int = 0
    var mediaType:String!
    var avatarLink:NSURL?
    var userName:String!
    
    var totalViews:Int = 0
    
    var progress:Float = 0.0
    var fileSize:Int64 = 0
    var downloadSize:Int64 = 0
    var timeRemain:Int = 0
    var speed:Int64 = 0
    
    var mediaStatus:MediaStatus!
    
    var delegate:ASMediaDelegate?
    
    var downloadOperation:AFHTTPRequestOperation?
    
    init(info:NSDictionary, type:MediaType) {
        
        super.init()
        mediaType = type.toValue()
        
        if type == MediaType.Audio {
            mediaID = String(info["id"] as Int)
            mediaTitle = info["title"] as String
            mediaDuration = info["duration"] as Int
            fileSize = Int64(info["original_content_size"] as Int)
            
            var userInfo = info["user"] as NSDictionary
            userName = userInfo["username"] as String
            
            if let count = info["playback_count"] as? Int {
                totalViews = count
            } 
            
            if let desc = info["description"] as? String {
                mediaDesc = desc
            }
            if let link = info["artwork_url"] as? String {
                avatarLink = NSURL(string: info["artwork_url"] as String)
            }
            if let url = info["stream_url"] as? String {
                mediaStreamUrl = info["stream_url"] as String
            }
            
        } else {
            var tempID = info["id"] as NSString
            var compnents = tempID.componentsSeparatedByString("/")
            mediaID = compnents.last as NSString
            mediaTitle = info["title"] as NSString
            userName = info["name"] as String

            if let desc = info["content"] as? String {
                mediaDesc = desc
            }
            
            var videoLinkStr = info["video"] as NSString
            var videoCompnent = videoLinkStr.componentsSeparatedByString("&")

            
            mediaStreamUrl = videoCompnent[0] as String
            avatarLink = NSURL(string: info["picture"] as NSString)
            
//            videoDate = NSDate.dateWithString(info["published"] as NSString, format: "yyyy-MM-dd'T'HH:mm:ss'.000Z")
//            videoFavCount = (info["favoriteCount"] as NSString).integerValue
            totalViews = (info["viewCount"] as NSString).integerValue
            mediaDuration = (info["duration"] as NSString).integerValue
            
        }
        
        mediaStatus = MediaStatus.None
    }
    
    func getStreamURL() -> NSURL {
        var downloadLink:String
        if mediaType == MediaType.Audio.toValue() {
            if ((ASMyFilesManager.sharedInstance.isItemAlreadyDownload(self)) != nil) {
                var savePath = String.documentDirctoryWithPath(self.mediaID + ".mp3")
                return NSURL(fileURLWithPath: savePath)!
            } else {
                downloadLink = mediaStreamUrl + "?client_id=" + clientID
            }
        } else {
            if ((ASMyFilesManager.sharedInstance.isItemAlreadyDownload(self)) != nil) {
                var savePath = String.documentDirctoryWithPath(self.mediaID + ".mp4")
                return NSURL(fileURLWithPath: savePath)!
            } else {
                downloadLink = "http://wv.smarnovative.com/YouTube-Downloader-master/getvideo.php?videoid=\(mediaID)&format=ipad"
            }
        }
        return NSURL(string: downloadLink)!
    }
    
    func fileSizeInString() -> String {
        
        var byteFromatter = NSByteCountFormatter.stringFromByteCount(fileSize, countStyle: NSByteCountFormatterCountStyle.File)
        
        return byteFromatter
    }
    
    func downloadMedia() {
        
        mediaStatus = MediaStatus.None
        
        var extenstion:String
        if mediaType == MediaType.Audio.toValue() {
            extenstion = ".mp3"
        } else {
            extenstion = ".mp4"
        }
        
        var request = NSURLRequest(URL: getStreamURL())
        
        println(request)
        
        var savePath = String.documentDirctoryWithPath(mediaID + extenstion)
        
        var operation = AFHTTPRequestOperation(request: request)
        
        operation.setShouldExecuteAsBackgroundTaskWithExpirationHandler({() -> () in
            operation.pause()
            self.mediaStatus = MediaStatus.pause
            println("Enter in background");
        })
        
        println("Save Path: " + savePath)
        
        operation.outputStream = NSOutputStream(toFileAtPath: savePath, append: false)
        
        var startTime = NSDate()
        var updateTimeInterval = 0.5
        operation.setDownloadProgressBlock({(var bytesRead:UInt, var totalBytesRead:Int64, var totalBytesExpectedToRead:Int64) -> Void in

            self.mediaStatus = MediaStatus.InProgress
            
            var now = NSDate()
            var timeInterval = now.timeIntervalSinceDate(startTime)
            if timeInterval > updateTimeInterval {
                updateTimeInterval += 0.5
                
                var remainTime = Double(totalBytesExpectedToRead)*timeInterval/Double(totalBytesRead) - timeInterval
                var bytesPerSec = Double(totalBytesRead)/timeInterval
                
                self.progress = Float(totalBytesRead)/Float(totalBytesExpectedToRead)
                self.fileSize = totalBytesExpectedToRead
                self.downloadSize = totalBytesRead
                self.timeRemain = Int(remainTime)
                self.speed = Int64(bytesPerSec)
                
                if self.delegate != nil {
                    self.delegate?.downloadReport(self.mediaStatus)
                }
            }
        })
        
        operation.setCompletionBlockWithSuccess({(var operation:AFHTTPRequestOperation!, var responseObject:AnyObject!) -> Void in
            
            NSLog("%@: Completed", self.mediaID)
            
            self.mediaStatus = MediaStatus.Completed
            
            ASDownloadManager.sharedInstance.removeItem(self)
            ASMyFilesManager.sharedInstance.saveMediaItems(self)
            
            NSNotificationCenter.defaultCenter().postNotificationName(DownloadCompleteNotification, object: self, userInfo: nil)
            
            }, failure: {(var operation:AFHTTPRequestOperation!, var error:NSError!) -> Void in
                
                self.mediaStatus = MediaStatus.Error
                if self.delegate != nil {
                    self.delegate?.downloadReport(self.mediaStatus)
                }
        });
        
        operation.start();
        
        downloadOperation = operation
    }
    
    func pauseOperation() {
        
        if let op = downloadOperation {
            op.pause()
            self.mediaStatus = MediaStatus.pause
        }
    }
    
    func resumeOperation() {
        if let op = downloadOperation {
            op.resume()
            self.mediaStatus = MediaStatus.InProgress
        }
    }
    func stopOperation() {
        if let op = downloadOperation {
            op.cancel()
            self.mediaStatus = MediaStatus.None
        }
    }
}

extension ASMedia {
    
    func encodeWithCoder(encoder: NSCoder) {
        encoder.encodeObject(mediaID, forKey: "mediaID")
        encoder.encodeObject(mediaTitle, forKey: "mediaTitle")
        encoder.encodeInteger(mediaDuration, forKey: "mediaDuration")
        
        encoder.encodeObject(mediaType, forKey: "mediaType")
        encoder.encodeObject(userName, forKey: "userName")
        encoder.encodeInt64(fileSize, forKey: "fileSize")
        encoder.encodeInteger(totalViews, forKey: "totalViews")
        
        if mediaDesc != nil {
            encoder.encodeObject(mediaDesc, forKey: "mediaDesc")
        }
        if avatarLink != nil {
            encoder.encodeObject(avatarLink, forKey: "avatarLink")
        }
        if mediaStreamUrl != nil {
            encoder.encodeObject(mediaStreamUrl, forKey: "mediaStreamUrl")
        }
    }
    
    func initWithCoder(decoder: NSCoder) -> ASMedia {
        
        self.mediaID = decoder.decodeObjectForKey("mediaID") as String
        self.mediaTitle = decoder.decodeObjectForKey("mediaTitle") as String
        self.mediaDuration = decoder.decodeIntegerForKey("mediaDuration")
        self.userName = decoder.decodeObjectForKey("userName") as String
        self.fileSize = decoder.decodeInt64ForKey("fileSize")
        self.totalViews = decoder.decodeIntegerForKey("totalViews")
        
        self.mediaType = decoder.decodeObjectForKey("mediaType") as String
        
        if let desc = decoder.decodeObjectForKey("mediaDesc") as? String {
            self.mediaDesc = desc
        }
        if let avatar = decoder.decodeObjectForKey("avatarLink") as? NSURL {
            self.avatarLink = avatar
        }
        if let steamUrl = decoder.decodeObjectForKey("mediaStreamUrl") as? String {
            self.mediaStreamUrl = steamUrl
        }
        
        return self
        
    }
}



