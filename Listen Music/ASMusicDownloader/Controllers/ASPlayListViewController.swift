//
//  ASPlayListViewController.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/30/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

class ASPlayListViewController: ASBaseViewController {

    @IBOutlet weak var fileList: UITableView!
    
    @IBOutlet weak var infoView:UIView!
    
    internal var downloads:NSMutableArray!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Recent Played"
        
        downloads = ASHistoryManager.sharedInstance.recentItems;
        self.infoView.hidden = downloads.count != 0
        self.fileList.tableFooterView = UIView(frame: CGRectZero)
    }
    

    func playMedia(media:ASMedia) {
        ASAudioPlayerManager.sharedInstance.playMedia(media)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func playAllItems() {
//        ASAudioPlayerManager.sharedInstance.addMediaList(downloads)
        self.navigationController?.popViewControllerAnimated(true)
    }

}

extension ASPlayListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.downloads.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var feedCell = tableView.dequeueReusableCellWithIdentifier("cell") as ASRSSFeedCell
        
        var media = downloads[indexPath.row] as ASMedia
        
        feedCell.feedTitle.text = media.mediaTitle
        feedCell.feedDetail.text = media.userName
        feedCell.feedDate.text = "Views: \(media.totalViews)"
        
        feedCell.avatar.image = UIImage(named: "browse_peer_audio_icon_selector_on")
        feedCell.avatar.cancelLoading()
        if let url = media.avatarLink {
            feedCell.avatar.imageURL = url
        }
        
        return feedCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedMedia = downloads[indexPath.row] as ASMedia
        playMedia(selectedMedia);        
    }
}

