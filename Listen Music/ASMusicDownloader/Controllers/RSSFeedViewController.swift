//
//  RSSFeedViewController.swift
//  Bilancio Sociale Provincia Cosenza
//
//  Created by Adnan Siddiq on 11/1/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit
import MediaPlayer

class RSSFeedViewController: ASBaseViewController {
    
    @IBOutlet weak var feedListView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var searchSegment:UISegmentedControl!
    
    internal var audioResult:NSArray!
    internal var videoResult:NSArray!
    internal var refreshControl:UIRefreshControl = UIRefreshControl()
    
    @IBOutlet weak var infoView:UIView!
    @IBOutlet weak var infoLabel:UILabel!
    @IBOutlet weak var retryButton:UIButton!
    
    var selectedIndex:Int = -1
    var selectedMedia:ASMedia!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        refreshControl.addTarget(self, action: Selector("refreshFeed"), forControlEvents: UIControlEvents.ValueChanged)
        self.feedListView.estimatedRowHeight = 100
        feedListView.addSubview(refreshControl)

        self.audioResult = NSArray()
        self.videoResult = NSArray()
        
        self.title = "Search"
        
        self.loadAudioFeed(nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "downloadComplete:", name: DownloadCompleteNotification, object: nil)
        
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.feedListView.reloadData()
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: DownloadCompleteNotification, object: nil)
    }
    
    func downloadComplete(notification:NSNotification) {
        self.feedListView.reloadData()
    }
        
    func refreshFeed() {
        var text = searchBar.text.terminatingWitheSpaceAndNewLine()
        if self.searchSegment.selectedSegmentIndex == 1 {
            if text.length == 0 {
                self.refreshVideoFeed(nil)
            } else {
                refreshVideoFeed(text)
            }
            
        } else {
            if text.length == 0 {
                refreshAudioFeed(nil)
            } else {
                refreshAudioFeed(text)
            }
        }
    }
    
    // MARK: - Video Searh API
    func loadVideoFeed(query:String?) {
        
        self.showProgressView(self.navigationController?.view, message: "Loading...")
        
        self.refreshVideoFeed(query)
    }
    
    func refreshVideoFeed(query:String?) {
        
        ASFeedManager.sharedInstance.cancelOperation()
        
        ASVideoManager.sharedInstance.loadFeed(query, finishBlock: {(feeds:NSArray?, error:NSError?) -> () in
            if let e = error {
                if e.code == -1011 {
                    self.infoLabel.text = "Search your favorite songs by tapping the search field at the top."
                    self.retryButton.hidden = true
                } else {
                    self.infoLabel.text = e.localizedDescription
                    self.retryButton.hidden = false
                }
                self.infoView.hidden = false
            } else {
                self.videoResult = feeds!
                self.feedListView.reloadData()
                if self.videoResult.count == 0 {
                    self.infoView.hidden = false
                    self.infoLabel.text = "No reasult found."
                    self.retryButton.hidden = true
                } else {
                    self.infoView.hidden = true
                }
            }
            self.hideProgress()
            self.refreshControl.endRefreshing()

        })
    }
    
    // MARK: - Audio Search API
    func loadAudioFeed(query:String?) {
        
        self.showProgressView(self.navigationController?.view, message: "Loading...")
        self.refreshAudioFeed(query)
    }
    
    func refreshAudioFeed(query:String?) {
        
        ASVideoManager.sharedInstance.cancelOperation()
        
        ASFeedManager.sharedInstance.loadFeed(query, finishBlock: {(feeds:NSArray?, error:NSError?) -> () in
            
            if let e = error {
                if e.code == -1011 {
                    self.infoLabel.text = "Search your favorite songs by tapping the search field at the top."
                    self.retryButton.hidden = true
                } else {
                    self.infoLabel.text = e.localizedDescription
                    self.retryButton.hidden = false
                }
                self.infoView.hidden = false
            } else {
                self.audioResult = feeds!
                self.feedListView.reloadData()
                if self.audioResult.count == 0 {
                    self.infoView.hidden = false
                    self.infoLabel.text = "No reasult found."
                    self.retryButton.hidden = true
                } else {
                    self.infoView.hidden = true
                }
            }
            self.hideProgress()
            self.refreshControl.endRefreshing()
        })
    }

    func configureCell(feedCell:ASRSSFeedCell, indexPath:NSIndexPath) {
        var temp:ASMedia
        if self.searchSegment.selectedSegmentIndex == 1 {
            temp = videoResult[indexPath.row] as ASMedia
        } else {
            temp = audioResult[indexPath.row] as ASMedia
        }
        
        feedCell.feedTitle.text = temp.mediaTitle
        feedCell.feedDate.text = temp.userName
        feedCell.totalViews.text = "Views: \(temp.totalViews)"
        
        feedCell.avatar.cancelLoading()
        feedCell.avatar.image = nil
        if let url = temp.avatarLink {
            feedCell.avatar.imageURL = url
        } else {
            if temp.mediaType == MediaType.Audio.toValue() {
                feedCell.avatar.image = UIImage(named: "browse_peer_audio_icon_selector_on")
            } else {
                feedCell.avatar.image = UIImage(named: "browse_peer_video_icon_selector_on")
            }
        }
        
        if ASDownloadManager.sharedInstance.isItemAlreadyDownloding(temp) {

            feedCell.downloadLabel.text = "Downloading..."
        } else if (ASMyFilesManager.sharedInstance.isItemAlreadyDownload(temp) != nil) {
            feedCell.downloadLabel.text = "Downloaded"
        } else {
            if temp.mediaType == MediaType.Audio.toValue() {
                feedCell.downloadLabel.text = byteFormatter().stringFromByteCount(temp.fileSize)
            } else {
                feedCell.downloadLabel.text = String.timeFormatted(temp.mediaDuration)
            }
        }
    }
    
    func hieghtForCell(indexPath:NSIndexPath) -> CGFloat {
        
        var feedCell = StaticStructure.feedCell
        
        if feedCell == nil {
            feedCell = self.feedListView.dequeueReusableCellWithIdentifier("cell") as ASRSSFeedCell
        }
        
        configureCell(feedCell, indexPath: indexPath)
        
        feedCell.bounds = CGRectMake(0.0, 0.0, CGRectGetWidth(self.feedListView.bounds), 0.0)
        feedCell.setNeedsLayout()
        feedCell.layoutIfNeeded()
        
        var size = feedCell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        
        return size.height
    }

    func playMedia(media:ASMedia) {
        if media.mediaType == MediaType.Video.toValue() {
            var mediaURL = media.getStreamURL()
            var player = MPMoviePlayerViewController(contentURL: mediaURL)
            self.presentMoviePlayerViewControllerAnimated(player)
        } else {
            ASAudioPlayerManager.sharedInstance.playMedia(media)
        }
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController.isKindOfClass(ASAudioPlayerViewController) {
            var vc = segue.destinationViewController as ASAudioPlayerViewController
            vc.media = sender as ASMedia
        }
    }
    
    func byteFormatter() -> NSByteCountFormatter {
        var byteFormatter = NSByteCountFormatter()
        byteFormatter.zeroPadsFractionDigits = true
        byteFormatter.adaptive = false
        byteFormatter.countStyle =  NSByteCountFormatterCountStyle.File
        return byteFormatter
    }

}

// MARK: - Selection Action

extension RSSFeedViewController {
    
    @IBAction func searchTypeAction(sender: AnyObject) {
        
        if self.searchSegment.selectedSegmentIndex == 0 {
            if searchBar.text.terminatingWitheSpaceAndNewLine().length == 0 {
                loadAudioFeed(nil)
            } else {
                loadAudioFeed(searchBar.text.terminatingWitheSpaceAndNewLine())
            }
        } else {
            if searchBar.text.terminatingWitheSpaceAndNewLine().length == 0 {
                loadVideoFeed(nil)
            } else {
                loadVideoFeed(searchBar.text.terminatingWitheSpaceAndNewLine())
            }
        }
    }
}

// MARK: - TableView Delegate

extension RSSFeedViewController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchSegment.selectedSegmentIndex == 1 {
            return self.videoResult.count
        } else {
            return self.audioResult.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var feedCell = tableView.dequeueReusableCellWithIdentifier("cell") as ASRSSFeedCell
        
        configureCell(feedCell, indexPath: indexPath)
        return feedCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.searchSegment.selectedSegmentIndex == 1 {
            selectedMedia = videoResult[indexPath.row] as ASMedia
        } else {
            selectedMedia = audioResult[indexPath.row] as ASMedia
        }
        
//        var actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Play")
        
        var actionAlert:UIActionSheet!

        if (ASDownloadManager.sharedInstance.isItemAlreadyDownloding(selectedMedia)) {
            actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Cancel")
            actionAlert.cancelButtonIndex = 1
            
        } else if ((ASMyFilesManager.sharedInstance.isItemAlreadyDownload(selectedMedia)) != nil) {
            actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Cancel")
            actionAlert.cancelButtonIndex = 1
        } else if selectedMedia.mediaStreamUrl != nil {
            actionAlert = UIActionSheet(title: selectedMedia.mediaTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Play", "Download", "Cancel")
            actionAlert.cancelButtonIndex = 2
        } else {
            UIAlertView(title: "", message: "Link is not valid")
        }
        
        if actionAlert != nil {
        
            actionAlert.showFromTabBar(self.tabBarController?.tabBar)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

struct StaticStructure {
    static var feedCell:ASRSSFeedCell!
}
extension RSSFeedViewController:UIActionSheetDelegate {
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        println("Action Sheet: \(buttonIndex)")
        
        if actionSheet.buttonTitleAtIndex(buttonIndex) == "Play" {
            playMedia(selectedMedia)
        } else if actionSheet.buttonTitleAtIndex(buttonIndex) == "Download" {
            ASDownloadManager.sharedInstance.downloadVideoItem(selectedMedia)
            self.feedListView.reloadData()
            
            if let tabvc = self.tabBarController {
                var temp = tabvc as MyTabbarController
                temp.setBadgeValue()
            }
        }
    }
    func actionSheetCancel(actionSheet: UIActionSheet) {
        println("Action Cancel")
    }
    
}

extension RSSFeedViewController:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        var text = searchBar.text.terminatingWitheSpaceAndNewLine()
        
        if text.length != 0 {
            if searchSegment.selectedSegmentIndex == 1 {
                loadVideoFeed(text)
            } else {
                self.loadAudioFeed(text)
            }
        }
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
}

