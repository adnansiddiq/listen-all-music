//
//  ASDownloadCell.swift
//  ASMusicDownloader
//
//  Created by Adnan Siddiq on 11/22/14.
//  Copyright (c) 2014 Adnan Siddiq. All rights reserved.
//

import UIKit

let downloadCellID = "cell_download"

class ASDownloadCell: UITableViewCell, ASMediaDelegate {
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var speedLabel:UILabel!
    @IBOutlet weak var progressBar:UIProgressView!
    @IBOutlet weak var sizeLabel:UILabel!
    @IBOutlet weak var mediaIcon:UIImageView!
    
    
    internal var cellMedia:ASMedia!


    func configureCell(media:ASMedia) {
     
        cellMedia = media

        self.titleLabel.text = media.mediaTitle
        
        if media.mediaType == MediaType.Audio.toValue() {
            self.mediaIcon.image = UIImage(named: "browse_peer_audio_icon_selector_on")
        } else {
            self.mediaIcon.image = UIImage(named: "browse_peer_video_icon_selector_on")
        }
        
        media.delegate = self
        downloadReport(media.mediaStatus)
}
    
    func downloadReport(status: MediaStatus) {
        
        var byteFormat = byteFormatter()
        
        switch status {
        case .None:
            self.sizeLabel.text = ""
            self.speedLabel.text = "Waiting..."
            self.progressBar.progress = cellMedia.progress
            self.progressBar.hidden = false
            self.sizeLabel.hidden = false
            self.speedLabel.textColor = UIColor.darkGrayColor()
            self.selectionStyle = UITableViewCellSelectionStyle.None
            break
        case .InProgress:
            self.sizeLabel.text = byteFormat.stringFromByteCount(cellMedia.downloadSize) + " of " + byteFormat.stringFromByteCount(cellMedia.fileSize)
            self.speedLabel.text = "Time Remains: " + self.formatTimeInSec(cellMedia.timeRemain) + " (" + byteFormat.stringFromByteCount(cellMedia.speed) + "/s)"
            self.progressBar.progress = cellMedia.progress
            
            self.progressBar.hidden = false
            self.sizeLabel.hidden = false
            self.speedLabel.textColor = UIColor.darkGrayColor()
            self.selectionStyle = UITableViewCellSelectionStyle.None
            break
        case .Completed:
            break
        case .Error:
            self.progressBar.hidden = true
            self.sizeLabel.hidden = true
            self.speedLabel.text = "Error: Tap to restart"
            self.speedLabel.textColor = UIColor.redColor()
            self.selectionStyle = UITableViewCellSelectionStyle.Default
            break
        default:
            break
        }
    }
    
    
    func byteFormatter() -> NSByteCountFormatter {
        var byteFormatter = NSByteCountFormatter()
        byteFormatter.zeroPadsFractionDigits = true
        byteFormatter.adaptive = false
        byteFormatter.countStyle =  NSByteCountFormatterCountStyle.File
        return byteFormatter
    }
    
    func formatTimeInSec(totalSeconds: Int) -> String {
        var seconds:Int = totalSeconds % 60;
        var minutes:Int = (totalSeconds / 60) % 60;
        var hours:Int = totalSeconds / 3600;
        let strHours = hours > 9 ? String(hours):"0" + String(hours)
        let strMinutes = minutes > 9 ? String(minutes):"0" + String(minutes)
        let strSeconds = seconds > 9 ? String(seconds):"0" + String(seconds)
        
        if hours > 0 {
            return "\(strHours)h \(strMinutes)m \(strSeconds)s"
        }
        else if minutes > 0 {
            return "\(strMinutes)m \(strSeconds)s"
        } else {
            return "\(strSeconds)s"
        }
    }

}
